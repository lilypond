\input texinfo @c -*- coding: utf-8; mode: texinfo; documentlanguage: ja -*-
@ignore
    Translation of GIT committish: 499a511d4166feaada31114e097f86b5e0c56421

    When revising a translation, copy the HEAD committish of the
    version that you are working on.  See TRANSLATION for details.
@end ignore

@documentencoding UTF-8
@setfilename lilypond-program.info
@settitle GNU LilyPond -- アプリケーション使用方法

@include macros.itexi

@afourpaper

@c don't remove this comment.
@ignore
@omfcreator Han-Wen Nienhuys, Jan Nieuwenhuizen and Graham Percival
@omfdescription Program Usage of the LilyPond music engraving system
@omftype user manual
@omfcategory Applications|Publishing
@omflanguage Japanese
@end ignore

@c Translators: Yoshiki Sawada
@c Translation status: post-GDP

@ifnottex
@node Top
@top GNU LilyPond --- アプリケーション使用方法
@end ifnottex

@ifhtml
@ifclear bigpage
このドキュメントは @uref{source/Documentation/user/lilypond-program.ja.pdf,PDF 
形式} と @uref{source/Documentation/user/lilypond-program-big-page.ja.html,
大きな 1 ページ形式} でも利用可能です。
@end ifclear
@ifset bigpage
このドキュメントは @uref{source/Documentation/user/lilypond-program.ja.pdf,PDF 
形式} と @uref{source/Documentation/user/lilypond-program/index.ja.html,
複数のページにインデックス化された形式} でも利用可能です。
@end ifset
@end ifhtml

@c urg, makeinfo 4.9.91 French translation is funky
@iftex
@documentlanguage ja
@c frenchspacing on
@end iftex

@syncodeindex fn cp
@c @syncodeindex ky cp
@c @syncodeindex pg cp
@c @syncodeindex tp cp
@c @syncodeindex vr cp

@finalout

@titlepage
@title GNU LilyPond
@c subtitle Le système de gravure musicale
@subtitle 音楽譜刻システム
@c titlefont{Utilisation des programmes}
@c author L'équipe de développement de LilyPond
@author LilyPond開発チーム

Copyright @copyright{} 1999--2008 著作者一同

@emph{The translation of the following copyright notice is provided
for courtesy to non-English speakers, but only the notice in English
legally counts.}

@emph{以下は英語を話さない人々のための著作権についての注意書きです。しかしなが@c
ら、英語で書かれた注意書きだけが法的に有効です。}

@quotation
GNU フリー文書利用許諾契約書バージョン 1.1 またはフリー ソフトウェア財団によっ@c
て発行されたその後のバージョンの約定に従う限り、このドキュメントを複製、変更す@c
る許可を与えます。変更不可部分はありません。この利用許諾契約書のコピーは 「GNU 
フリー文書利用許諾契約書」
という章に含まれています。
@end quotation

@quotation
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.1
or any later version published by the Free Software Foundation;
with no Invariant Sections.
A copy of the license is included in the section entitled ``GNU
Free Documentation License''.
@end quotation

@vskip 20pt

For LilyPond version @version{}
@end titlepage


@ifnottex
このファイルは GNU LilyPond プログラムの使用方法についてのドキュメントです。
@c This file documents GNU LilyPond program usage.

Copyright @copyright{} 1999--2008 著作者一同

@emph{The translation of the following copyright notice is provided
for courtesy to non-English speakers, but only the notice in English
legally counts.}

@emph{以下は英語を話さない人々のための著作権についての注意書きです。しかしなが@c
ら、英語で書かれた注意書きだけが法的に有効です。}


@quotation
GNU フリー文書利用許諾契約書バージョン 1.1 またはフリー ソフトウェア財団によっ@c
て発行されたその後のバージョンの約定に従う限り、このドキュメントを複製、変更す@c
る許可を与えます。変更不可部分はありません。この利用許諾契約書のコピーは 「GNU 
フリー文書利用許諾契約書」
という章に含まれています。
@end quotation

@quotation
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.1
or any later version published by the Free Software Foundation;
with no Invariant Sections.
A copy of the license is included in the section entitled ``GNU
Free Documentation License''.
@end quotation
@end ifnottex


@ifnottex

これは GNU LilyPond バージョン @version{} のためのアプリケーション使用方法マ@c
ニュアルです。このドキュメントと他のドキュメントとの関係についての更なる情報@c
は、@rlearning{About the documentation} を参照してください。

@cindex web site
@cindex URL

@uref{http://@/www@/.lilypond@/.org/} で更なる情報を見つけることができます。こ@c
のウェブ サイトにはこのドキュメントと他のドキュメントのオンライン コピーがあり@c
ます。

@include dedication.itely

@menu
* Install::                        インストール方法とコンパイル方法
* Setup::                          他のプログラムの助けを借りて LilyPond を使@c
用する
* Running LilyPond::               操作方法
* LilyPond-book::                  テキストと音楽を統合する
* Converting from other formats::  lilypond ソース形式に変換する

付録

* GNU Free Documentation License:: このドキュメントの使用許諾書
* LilyPond index::
@end menu
@end ifnottex

@contents


@include install.itely
@include setup.itely
@include running.itely
@include lilypond-book.itely
@include converters.itely

@include fdl.itexi

@node LilyPond index
@appendix LilyPond index

@printindex cp

@bye
