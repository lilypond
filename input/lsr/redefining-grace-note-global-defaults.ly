%% Do not edit this file; it is auto-generated from LSR http://lsr.dsi.unimi.it
%% This file is in the public domain.
\version "2.13.1"

\header {
  lsrtags = "rhythms"

  texidoces = "

Los valores predeterminados para las notas de adorno están
almacenados en los identificadores @code{startGraceMusic},
@code{stopGraceMusic}, @code{startAcciaccaturaMusic},
@code{stopAcciaccaturaMusic}, @code{startAppoggiaturaMusic} y
@code{stopAppoggiaturaMusic}, que están definidos en el archivo
@code{ly/grace-init.ly}.  Redefiniéndolos se pueden obtener otros
efectos.

"

  doctitlees = "Redefinición de los valores globales predeterminados para notas de adorno"

  texidoc = "
The global defaults for grace notes are stored in the identifiers
@code{startGraceMusic}, @code{stopGraceMusic},
@code{startAcciaccaturaMusic}, @code{stopAcciaccaturaMusic},
@code{startAppoggiaturaMusic} and @code{stopAppoggiaturaMusic}, which
are defined in the file @code{ly/grace-init.ly}.  By redefining them
other effects may be obtained.

"
  doctitle = "Redefining grace note global defaults"
} % begin verbatim

startAcciaccaturaMusic = {
  s1*0(
  \override Stem #'stroke-style = #"grace"
  \slurDashed
}

stopAcciaccaturaMusic = {
  \revert Stem #'stroke-style
  \slurSolid
  s1*0)
}

\relative c'' {
  \acciaccatura d8 c1
}
