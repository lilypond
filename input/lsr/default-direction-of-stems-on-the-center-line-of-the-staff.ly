%% Do not edit this file; it is auto-generated from LSR http://lsr.dsi.unimi.it
%% This file is in the public domain.
\version "2.13.1"

\header {
  lsrtags = "editorial-annotations"

  texidoces = "
La dirección predeterminada de las plicas sobre la tercera línea
del pentagrama está determinada por la propiedad
@code{neutral-direction} del objeto @code{Stem}.

"
  doctitlees = "Dirección predeterminada de las plicas sobre la tercera línea del pentagrama"

%% Translation of GIT committish :0364058d18eb91836302a567c18289209d6e9706
  texidocde = "
Die Richtung von Hälsen auf der mittleren Linie kann mit der @code{Stem}-Eigenschaft
@code{neutral-direction} gesetzt werden.

"
  doctitlede = "Standardrichtung für Hälse auf der Mittellinie"

  texidoc = "
The default direction of stems on the center line of the staff is set
by the @code{Stem} property @code{neutral-direction}.

"
  doctitle = "Default direction of stems on the center line of the staff"
} % begin verbatim

\relative c'' {
  a4 b c b
  \override Stem #'neutral-direction = #up
  a4 b c b
  \override Stem #'neutral-direction = #down
  a4 b c b
}
